# README #

This is simple Gtk+ ComboBox demo.
It demonstrates how to show and icon and a text in Combo Box.
It is written in JavaScript and you need ```gjs``` to run it


### How to run ###
In project's directory do:

```
$ gjs main.js
```