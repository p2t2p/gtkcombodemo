const GObject = imports.gi.GObject
const Gio = imports.gi.Gio
const AppInfo = Gio.AppInfo
const Gtk = imports.gi.Gtk
const Lang = imports.lang


const format = function() {
	const args = Array.prototype.slice.call(arguments)

	let msg = args[0]
	const items = args.slice(1)

	items.forEach((item) => {
		msg = msg.replace('{}', item)
	})

	return msg
}

const log = function() {
	const msg = format.apply(null, Array.prototype.slice.call(arguments))
	print('[DEMO]: ' + msg + '\n')
}

const buttons = (function(){
	const capitalize = function(string) {
		return string.charAt(0).toUpperCase() + string.slice(1)
	}

	const create = function(name, onClick){
		const capitalName = capitalize(name)

		return new Lang.Class({
			Name: 'ComboDemoButtons' + capitalName,

			_init: function(builder, widget) {
				this.widget = widget
				this.button = this.initButton(builder)
			},

			initButton: function(builder) {
				const button = builder.get_object(format('settings-{}-button', name))
				button.connect('clicked', Lang.bind(this, this.clicked))
				return button
			},

			clicked: function() {
				onClick(this.widget)
			}
		})
	}

	return {
		Add: create('add', (window) => {
			const widget = new Item()

			window.list.insert(widget, -1)
		})
	}
})()

const apps = function(){
	return new (function(){
		const self = {
			all: function() {
				return AppInfo.get_all().map((info) => {
					const app = {
						icon: info.get_icon(),
						id: info.get_id(),
						name: info.get_name(),
						displayName: info.get_display_name(),
						commandline: info.get_commandline()
					};

					return app
				})
			}
		}

		return self
	})()
}

const Combo = new Lang.Class({
	Name: 'Snap.Widgets.Combos.Apps',
	GTypeName: 'SnapWidgetsCombosApp',

	_init: function(builder, window) {
		this.window = window
		this.model = this.initAppModel(builder)
		this.combo = this.initAppCombo(builder, this.model)
	},

	initAppCombo: function(builder, model) {
		const combo = builder.get_object('shortcut-app-combo')

		combo.set_model(model)

		const iconRenderer = new Gtk.CellRendererPixbuf()
		const textRenderer = new Gtk.CellRendererText()

		combo.pack_start(iconRenderer, false)
		combo.pack_end(textRenderer, false)

		combo.add_attribute(iconRenderer, 'gicon', 1)
		combo.add_attribute(textRenderer, 'text', 2)

		combo.set_active(0)


		this.comboHandler = combo.connect('changed', Lang.bind(this, this.onComboChange))

		return combo
	},

	initAppModel: function(builder) {
		const model = new Gtk.ListStore();
        model.set_column_types ([
            GObject.TYPE_STRING,
            Gio.Icon,
            GObject.TYPE_STRING]);

		apps().all().forEach((app) => {
			if(app.icon && app.name && app.name.length > 0) {
				const iter = model.append()
				model.set(iter, [0], [app.id])
				model.set(iter, [1], [app.icon])
				model.set(iter, [2], [app.name])
			}
		})

		return model
	},

	onComboChange: function() {
		const id = this.combo.get_active_id()
		log('App selected: {}', this.combo.get_active_id())
	}
})

const Item = new Lang.Class({
	Name: 'ComboBoxDemoItem',
	Extends: Gtk.ListBoxRow,

	_init: function() {
		this.parent({})

		this.builder = this.initBuilder()
		this.mainWidget = this.initMainWidget(this.builder)
		this.appsCombo = new Combo(this.builder, this)

		this.add(this.mainWidget)
		this.show_all()
	},

	initKeymap: function() {
		log('Preparing keymap...')
		const display = Gdk.Display.get_default(0)
		const  keymap = Gdk.Keymap.get_for_display(display)

		return keymap
	},

	initBuilder: function(){
		log('Preparing UI builder...')
		const builder = new Gtk.Builder()
		builder.add_from_file('./ui/item.glade')

		return builder
	},

	initMainWidget: function(builder){
		log('Preparing main widget...')
		const mainWidget = builder.get_object('shortcut-box')
		return mainWidget
	}
})

const List = new Lang.Class({
	Name: 'ComboDemoList',
	Extends: Gtk.Box,

	_init: function(window) {
		this.parent({})

		this.hexpand = true
		this.vexpand = true
		this.hexpand_set = true
		this.vexpand_set = true
		this.halign = Gtk.Align.FILL
		this.valign = Gtk.Align.FILL

		this.builder = this.initBuilder()

		this.mainWidget = this.initMainWidget(this.builder)
		this.list = this.initList(this.builder)
		this.addButton = new buttons.Add(this.builder, this)

		this.show_all()
		window.add(this)
	},

	initBuilder: function() {
		const builder = new Gtk.Builder()
		builder.add_from_file('./ui/list.glade')
		return builder;
	},

	initMainWidget: function(builder) {
		const widget = builder.get_object('settings-box')
		this.add(widget)
		return widget
	},

	initList: function(builder) {
		const list = builder.get_object('shortcuts-list')
		return list
	}
})

const Window  = new Lang.Class({
	Name: 'Combo.Demo.Window',

	_init: function(app) {
		this.window = this.initWindow(app)
		this.list = this.initList(this.window)
	},

	initWindow: function(app) {
		const window = new Gtk.ApplicationWindow({
			application: app,
			window_position: Gtk.WindowPosition.CENTER,
			title: "Combo Box Demo",
			default_width: 200,
			border_width: 10
		})

		app.connect('activate', Lang.bind(this, this.onActivate))

		return window
	},

	initList: function(window) {
		const list = new List(window)
		return list
	},

	onActivate: function() {
		this.window.present()
	},
})

const App = new Lang.Class({
	Name: 'ComboDemoApp',

	_init: function() {
		this.app = this.initApp()
	},

	initApp: function(){
		const app = new Gtk.Application({
			application_id: 'org.example.demo.combo'
		})

		app.connect('startup', Lang.bind(this, this.onStartup))

		return app
	},

	onStartup: function(){
		this.window = this.initWindow(this.app)
	},

	initWindow: function(app) {
		const window = new Window(app)
		return window
	},

	run: function(argv) {
		this.app.run(argv)
	}
})

const app = new App()
app.run(ARGV)

